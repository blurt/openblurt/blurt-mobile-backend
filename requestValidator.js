const RequestIp = require("@supercharge/request-ip");
var blurt = require("@blurtfoundation/blurtjs");

function validateRequest(req) {
  // console.log('public_key: ' + req.headers['public_key']);
  // console.log('memo: ' + req.headers['memo']);
  if (req.headers["public_key"] === undefined) {
    return null;
  }
  if (req.headers["memo"] === undefined) {
    return null;
  }
  const text = req.headers["memo"];
  const memo = blurt.memo.decode(process.env.PRIVATE_MEMO_KEY, text);
  const memoItems = memo.split("___");
  if (memoItems.length !== 3) {
    return null;
  }
  const ip = RequestIp.getClientIp(req);
  if (ip === undefined) {
    return null;
  }
  if (ip !== memoItems[1]) {
    return null;
  }
  if (req.headers["public_key"] !== memoItems[2]) {
    return null;
  }
  return "ok";
}

function validateCreateRequest(req) {
  if (req.headers["public_key"] === undefined) {
    return null;
  }
  if (req.headers["memo"] === undefined) {
    return null;
  }
  const text = req.headers["memo"];
  const memo = blurt.memo.decode(process.env.PRIVATE_MEMO_KEY, text);
  const memoItems = memo.split("___");
  if (memoItems.length !== 5) {
    return null;
  }
  const ip = RequestIp.getClientIp(req);
  if (ip === undefined) {
    return null;
  }
  if (ip !== memoItems[1]) {
    return null;
  }
  if (req.headers["public_key"] !== memoItems[2]) {
    return null;
  }
  if (req.headers["twitterusername"] !== memoItems[3]) {
    return null;
  }
  if (req.headers["username"] !== memoItems[4]) {
    return null;
  }
  return "ok";
}

function validateUsernameRequest(req) {
  if (req.headers["public_key"] === undefined) {
    return null;
  }
  if (req.headers["memo"] === undefined) {
    return null;
  }
  const text = req.headers["memo"];
  const memo = blurt.memo.decode(process.env.PRIVATE_MEMO_KEY, text);
  const memoItems = memo.split("___");
  if (memoItems.length !== 4) {
    return null;
  }
  const ip = RequestIp.getClientIp(req);
  if (ip === undefined) {
    return null;
  }
  if (ip !== memoItems[1]) {
    return null;
  }
  if (req.headers["public_key"] !== memoItems[2]) {
    return null;
  }
  if (req.headers["twitterusername"] !== memoItems[3]) {
    return null;
  }
  return "ok";
}

module.exports = {
  validateRequest,
  validateCreateRequest,
  validateUsernameRequest
}