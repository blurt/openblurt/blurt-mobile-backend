require("dotenv").config();
var express = require("express");
var app = express();
var blurt = require("@blurtfoundation/blurtjs");
var moment = require("moment");
const RequestIp = require("@supercharge/request-ip");
var Twitter = require("twitter");
const CryptoJS = require("crypto-js");

const { validateRequest,  validateCreateRequest, validateUsernameRequest} = require("./requestValidator");

const client = new Twitter({
  consumer_key: process.env.TWITTER_API_KEY,
  consumer_secret: process.env.TWITTER_API_KEY_SECRET,
  access_token_key: process.env.TWITTER_ACCESS_TOKEN,
  access_token_secret: process.env.TWITTER_ACCESS_TOKEN_SECRET,
});

// To understand the APIs, please refer to following document
// https://blurt.blog/blurt/@sagarkothari88/blurt-mobile-app-sign-up-3rd-week-sep-2022

/*
App -> Server: Initiate sign-up request

note:
- App inform Server about initiating sign-up process
- App sends public key of App (different public key for each sign-up session)
- Server in response, returns public_key of server.
*/
app.get("/getPublicKey", async function (req, res) {
  // console.log('public_key: ' + req.query.public_key);
  if (req.query.public_key === undefined) {
    res.status(401).send("Bad request");
  }
  const ip = RequestIp.getClientIp(req);
  console.log(ip);
  const text = `#${moment().utcOffset(0).format()}___${ip}___${
    req.query.public_key
  }`;
  const memo = blurt.memo.encode(
    process.env.PRIVATE_MEMO_KEY,
    process.env.PUBLIC_MEMO_KEY,
    text
  );
  res.send({ memo: memo, publicKey: process.env.PUBLIC_MEMO_KEY });
});

/*
App -> Server: Twitter username

note:
- App encodes twitter username, IP address with server-public-key
- Sends it to Server
*/
app.get("/check", async function (req, res) {
  if (validateUsernameRequest(req) === undefined) {
    res.status(401).send("Bad request");
  } else {
    if (req.headers["twitterusername"] === undefined) {
      res.status(401).send("Did not provide twitter username");
    } else {
      var params = { screen_name: req.headers["twitterusername"] };
      client.get("users/show", params, function (error, response) {
        if (!error) {
          const data = response.created_at;
          if (data === undefined) {
            res.status(401).send("Low Twitter Account Score");
          } else {
            const date = moment(response.created_at);
            const currentDate = moment();
            const threeMonths = moment(currentDate).subtract(3, "months");
            const result = moment(date).isBefore(threeMonths);
            if (result === true) {
              const text = req.headers["memo"];
              const memo = blurt.memo.decode(
                process.env.PRIVATE_MEMO_KEY,
                text
              );
              const newMemo = `${memo}___${req.headers["twitterusername"]}`;
              const newEncodedMemo = blurt.memo.encode(
                process.env.PRIVATE_MEMO_KEY,
                process.env.PUBLIC_MEMO_KEY,
                newMemo
              );
              res.send({
                memo: newEncodedMemo,
                publicKey: process.env.PUBLIC_MEMO_KEY,
              });
            } else {
              res.status(401).send("Low Twitter Account Score");
            }
          }
        } else {
          res.status(401).send("Bad request");
        }
      });
    }
  }
});

function createAccount() {
  const array = CryptoJS.lib.WordArray.random(10);
  return 'P' + PrivateKey.fromSeed(array.toString()).toString();
}

/*
App -> Server: Blurt Username

note:
- App sends BLURT username to server
- Server validates - IP address, twitterusername, BLURT username, etc.
- If everything looks good, Server creates account, encodes all data with app's public key (which only app can decode)
- Server sends that data back to app
*/
app.get("/checkAccountName", async function (req, res) {
  if (validateCreateRequest(req) === undefined) {
    res.status(401).send("Bad request");
  } else {
    if (
      req.headers["twitterusername"] === undefined ||
      req.headers["username"] === undefined
    ) {
      res.status(401).send("Did not provide username");
    } else {
      var expectedUsername = req.headers["username"];
      blurt.api.getAccounts([expectedUsername], function (err, response) {
        console.log(err, response);
        if (err) {
          res.status(401).send(`Error from blurt chain - ${err}`);
        } else {
          if (Array.isArray(response) && response.length === 0) {
            const text = req.headers["memo"];
            const memo = blurt.memo.decode(
              process.env.PRIVATE_MEMO_KEY,
              text
            );
            const items = memo.split("___");
            const ip = items[0];
            const datetime = items[1];
            const publicKey = items[2];
            const twitterUsername = items[3];
            const expectedUsername = items[4];
            // THIS IS WHERE I NEED HELP.
            blurt.broadcast.accountCreateWithDelegation(process.env.PRIVATE_ACTIVE_KEY, fee, delegation, creator, newAccountName, owner, active, posting, memoKey, jsonMetadata, extensions, function(err, result) {
              console.log(err, result);
            });
            const response = JSON.stringify({ip, datetime, publicKey, twitterUsername, expectedUsername});
            const newMemo = `#${response}`;
            const newEncodedMemo = blurt.memo.encode(
              process.env.PRIVATE_MEMO_KEY,
              process.env.PUBLIC_MEMO_KEY,
              newMemo
            );
            res.send({
              memo: newEncodedMemo,
              publicKey: process.env.PUBLIC_MEMO_KEY,
            });
          } else {
            res.status(401).send(`Blurt username not available`);
          }
        }
      });
    }
  }
});

// TO-DO: work on this api
app.get("/checkTweet", async function (req, res) {
  if (validateRequest(req) === undefined) {
    res.status(401).send("Bad request");
  } else {
    if (
      req.headers["twitterusername"] === undefined ||
      req.headers["username"] === undefined
    ) {
      res.status(401).send("Did not provide username");
    } else {
      var expectedUsername = req.headers["username"];
      blurt.api.getAccounts([expectedUsername], function (err, response) {
        console.log(err, response);
        if (err) {
          res.status(401).send(`Error from blurt chain - ${err}`);
        } else {
          if (Array.isArray(response) && response.length === 0) {
            const text = req.headers["memo"];
            const memo = blurt.memo.decode(
              process.env.PRIVATE_MEMO_KEY,
              text
            );
            const items = memo.split("___");
            const ip = items[0];
            const datetime = items[1];
            const publicKey = items[2];
            const twitterUsername = items[3];
            const response = JSON.stringify({ip, datetime, publicKey, twitterUsername, expectedUsername});
            const newMemo = `#${response}`;
            const newEncodedMemo = blurt.memo.encode(
              process.env.PRIVATE_MEMO_KEY,
              process.env.PUBLIC_MEMO_KEY,
              newMemo
            );
            res.send({
              memo: newEncodedMemo,
              publicKey: process.env.PUBLIC_MEMO_KEY,
            });
          } else {
            res.status(401).send(`Blurt username not available`);
          }
        }
      });
    }
  }
});

console.log(`${createAccount()}`);

app.listen(5002, () => {
  console.log("Server is Running");
});
