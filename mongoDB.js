const mongoose = require("mongoose");
const uri = "mongodb://localhost:27017/blurtsignupbackend";
mongoose.connect(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

const IPAddressSchema = new mongoose.Schema({
  ipaddressused: {
    type: String,
    index: true,
  },
  dateOfFirstAccess: {
    type: Date,
    default: Date.now,
  },
});

const TwitterUserNameSchema = new mongoose.Schema({
  twitternameused: {
    type: String,
    index: true,
  },
  dateOfFirstAccess: {
    type: Date,
    default: Date.now,
  },
});

const IPAddress = mongoose.model("IPAddress", IPAddressSchema);
const TwitterUserName = mongoose.model(
  "TwitterUserName",
  TwitterUserNameSchema
);
module.exports = {
  IPAddress,
  TwitterUserName,
};
